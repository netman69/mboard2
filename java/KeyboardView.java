package com.example.netman.mboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Handler;
import android.os.Vibrator;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.Gravity;
import java.util.ArrayList;
import java.lang.Math;
import java.io.InputStream;
import org.json.JSONObject;
import org.json.JSONArray;

import android.util.Log;

// TODO
// - Refactor.
// - Settings.
// - Auto capitalization.
// - Superscript text on Key class.
// - Images on Key class.
// - Numbers/symbols.
// - Longpress is different key.
// - Pick from multiple things when longpress for symbol.

public class KeyboardView extends LinearLayout {
	private Context ctx;
	private IMEService svc;
	private ArrayList<Key> keys = new ArrayList<Key>();
	private Key keyCurrent = null;
	private Vibrator v; // TODO handle better
	private boolean shift = false;
	private KeyPopup popup;

	public KeyboardView(Context context) {
		super(context);
		ctx = context;
		init();
	}

	public KeyboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ctx = context;
		init();
	}

	public KeyboardView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		ctx = context;
		init();
	}

	private void init() {
		v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
		setPadding(5, 1, 5, 1); // TODO Setting for this.
		popup = new KeyPopup(ctx);
	}

	public void setIMEService(IMEService svc) {
		this.svc = svc;
	}

	private boolean isInside(View v, MotionEvent ev) {
		float x = ev.getRawX(), y = ev.getRawY();
		int pos[] = new int[2];
		v.getLocationOnScreen(pos);
		return pos[0] + v.getWidth() > x &&  // right edge
		       pos[1] + v.getHeight() > y && // bottom edge
		       pos[0] < x &&                 // left edge
		       pos[1] < y;                   // top edge
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		Key keyNew = null;
		for (Key key : keys) {
			if (isInside(key, ev))
				keyNew = key;
		}
		switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				keyCurrent = keyNew;
				if (keyCurrent != null)
					keyCurrent.enter();
				return true;
			case MotionEvent.ACTION_UP:
				if (keyCurrent != keyNew) {
					if (keyCurrent != null)
						keyCurrent.exit();
					keyCurrent = keyNew;
					if (keyCurrent != null)
						keyCurrent.enter();
				}
				if (keyCurrent != null) {
					keyCurrent.activate();
					keyCurrent.exit();
				}
				return true;
			case MotionEvent.ACTION_MOVE:
				if (keyCurrent != keyNew) {
					if (keyCurrent != null)
						keyCurrent.exit();
					keyCurrent = keyNew;
					if (keyCurrent != null)
						keyCurrent.enter();
				}
				return true;
		}
		return false;
	}

	public void setLayout() {
		String json = null;
		try {
			InputStream is = ctx.getResources().openRawResource(R.raw.layout_us); // TODO User choosable layouts.
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			json = new String(buffer, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace(); // TODO, handle this better
			return;
		}

		try {
			JSONArray rows = new JSONArray(json);
			for (int i = 0; i < rows.length(); ++i) {
				JSONArray keys = rows.getJSONArray(i);
				LinearLayout row = new Row();
				for (int j = 0; j < keys.length(); ++j) {
					JSONObject key = keys.getJSONObject(j);
					String type = key.getString("type");
					float width;
					try {
						width = (float) key.getDouble("width");
					} catch(Exception e) {
						width = 1.0f;
					}
					switch (type) {
						case "key":
							String normal = key.getString("normal"), shifted = null;
							try {
								shifted = key.getString("shift");
							} catch (Exception e) { }
							row.addView(new Key(normal, shifted));
							break;
						case "shift":
							row.addView(new Shift(width));
							break;
						case "spacer":
							row.addView(new Spacer(width));
							break;
						case "backspace":
							row.addView(new BackSpace(width));
							break;
						case "space":
							row.addView(new Space());
							break;
						case "switch":
							row.addView(new Switch());
							break;
						case "enter":
							row.addView(new Enter());
							break;
					}
				}
				addView(row);
			}
		} catch (Exception e) {
			e.printStackTrace(); // TODO, handle this better
			return;
		}
	}

	public void setShift(boolean v) {
		shift = v;
		for (Key key : keys)
			key.updateShift();
	}

	private class Row extends LinearLayout {
		public Row() {
			super(ctx);
			setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
			setOrientation(LinearLayout.HORIZONTAL);
		}
	}

	private class Key extends TextView {
		String normal, shifted;

		public Key(String text) {
			super(ctx);
			init(text, null, 1.0f);
		}

		public Key(String normal, String shifted) {
			super(ctx);
			init(normal, shifted, 1.0f);
		}

		public Key(String text, float weight) {
			super(ctx);
			init(text, null, weight);
		}

		public Key(String normal, String shifted, float weight) {
			super(ctx);
			init(normal, shifted, weight);
		}

		private void init(String normal, String shifted, float weight) {
			this.normal = normal;
			this.shifted = shifted;
			updateShift();
			setTextSize(20f / getResources().getDisplayMetrics().density);
			setFocusable(false);
			setFocusableInTouchMode(false);
			setTransformationMethod(null); // To stop letters being all-caps.
			setAllCaps(false);
			setTextAlignment(TEXT_ALIGNMENT_CENTER);
			setPadding(5, 5, 5, 5); /* Left, Top, Right, Bottom */
			setBackgroundResource(R.drawable.key);
			setTextColor(getResources().getColor(R.color.colorPrimary));
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, weight);
			params.setMargins(0, 0, 0, 0);
			setLayoutParams(params);
			keys.add(this);
		}

		public void enter() {
			setTextColor(getResources().getColor(R.color.colorAccent));
			popup.setText(getText());
			// TODO Don't hardcode that -20.
			popup.showAtLocation(this, Gravity.LEFT | Gravity.TOP, getLeft() + ((View) getParent()).getLeft(), ((View) getParent()).getTop() - 20);
		}

		public void exit() {
			setTextColor(getResources().getColor(R.color.colorPrimary));
			popup.dismiss();
		}

		public void activate() {
			v.vibrate(50); // TODO Have length configurable.
			svc.insert(getText().toString());
			setShift(false);
		}

		public void updateShift() {
			if (shift && shifted != null)
				setText(shifted);
			else setText(normal);
		}
	}

	private class Switch extends Key {
		public Switch() {
			super("::", 1.0f);
		}

		@Override
		public void activate() {
			svc.switchIme();
		}
	}

	private class Enter extends Key {
		public Enter() {
			super("#", 1.0f);
		}

		@Override
		public void activate() {
			svc.enter();
		}
	}

	private class Shift extends Key {
		public Shift(float weight) {
			super("^^", "!!", weight);
		}

		@Override
		public void activate() {
			setShift(!shift); // TODO doubletap to lock
		}
	}

	private class Space extends Key {
		private int move_treshold = 10;
		private float move_multiplier = 0.1f;
		private float startx;
		private int startc;
		private boolean moving;

		public Space() {
			super(" ", 0.5f);
		}

		@Override
		public boolean onTouchEvent(MotionEvent ev) {
			switch (ev.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startx = ev.getX();
					moving = false;
					enter();
					return true;
				case MotionEvent.ACTION_UP:
					if (!moving)
						activate();
					exit();
					return true;
				case MotionEvent.ACTION_MOVE:
					if (!moving && Math.abs(ev.getX() - startx) > move_treshold) {
						moving = true; // TODO option to disable this feature
						ExtractedText et = svc.getCurrentInputConnection().getExtractedText(new ExtractedTextRequest(), 0);
						startc = et.startOffset + et.selectionEnd;
					} else if (moving) {
						int pos = startc + (int) ((ev.getX() - startx) * move_multiplier);
						svc.getCurrentInputConnection().setSelection(pos, pos);
					}
					return true;
			}
			return false;
		}
	}

	private class BackSpace extends Key {
		private int repeat_delay = 500; // TODO make configurable
		private int repeat_interval = 100; // TODO make configurable
		private float select_multiplier = 0.1f; // TODO make configurable
		private int start = 0, end = 0;
		private Handler handler = new Handler();
		private boolean activated, selecting;
		private Runnable repeat = new Runnable() {
			@Override 
			public void run() {
				activate();
				handler.postDelayed(repeat, repeat_interval);
			}
		};

		public BackSpace(float weight) {
			super("<-", weight);
		}

		public void enter() {
			setTextColor(getResources().getColor(R.color.colorAccent));
		}

		public void exit() {
			setTextColor(getResources().getColor(R.color.colorPrimary));
		}

		public void activate() {
			v.vibrate(50); // TODO configurable length
			activated = true;
			svc.backspace();
		}

		@Override
		public boolean onTouchEvent(MotionEvent ev) {
			switch (ev.getAction()) {
				case MotionEvent.ACTION_DOWN:
					activated = false;
					selecting = false;
					handler.postDelayed(repeat, repeat_delay); // TODO maybe option to turn off
					enter();
					return true;
				case MotionEvent.ACTION_UP:
					handler.removeCallbacks(repeat);
					if (selecting || !activated) // So that when stopping auto-repeat, it stops immediately.
						activate();
					exit();
					return true;
				case MotionEvent.ACTION_MOVE:
					if (!selecting && ev.getX() < 0) {
						handler.removeCallbacks(repeat);
						selecting = true; // TODO setting to turn this on/off
						// TODO also delete selected text when pressing backspace with stuff selected
						ExtractedText et = svc.getCurrentInputConnection().getExtractedText(new ExtractedTextRequest(), 0);
						start = et.startOffset + et.selectionStart;
						end = et.startOffset + et.selectionEnd;
						/* // This prevents visual bug that happens on my GN4, don't know if it matters.
						svc.getCurrentInputConnection().commitText(et.text.subSequence(et.selectionStart, et.selectionEnd), 1);
						svc.getCurrentInputConnection().setSelection(start, end);*/
					} else if (selecting) {
						int s = start + (int) (ev.getX() * select_multiplier);
						svc.getCurrentInputConnection().setSelection((s < start) ? s : start, end);
					}
					return true;
			}
			return false;
		}
	}

	private class Spacer extends Key {
		public Spacer(float weight) {
			super("", weight);
			setVisibility(INVISIBLE);
		}
	}
}
