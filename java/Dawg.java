package com.example.netman.mboard;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Dawg {
	private MHashMap<String, Integer> scores = new MHashMap<String, Integer>();
	private DedupMap<Node> nodes = new DedupMap<Node>();
	private Node root = new Node();

	public class Result {
		public String word;
		public int score, dist;
		public boolean isPrefix;
	}

	private class Node {
		private Node[] children = new Node[0]; // TODO I think having two arrays eats significantly more memory, verify this (60.54 vs 51.45).
		private int[] children_idx = new int[0];
		private boolean isEnd = false; // TODO just check words list hashmap instead of this?
		private int refc = 0;

		private void incRefc() {
			if (++refc == 1)
				nodes.add(this, true, false);
		}

		private void decRefc() { // Warning, this could potentially overflow the stack (it's recursive).
			if (--refc == 0) {
				nodes.remove(this, true, false);
				for (Node c : children)
					c.decRefc();
			}
		}

		public void setChild(int key, Node value) { // Warning, potential stack overflow and this changes the hashCode.
			nodes.remove(this, true, false);
			value.incRefc(); // If we are overwriting the same value, this is decremented later and prevents refc falling to 0 in that case.
			for (int i = 0; i < children.length; ++i) {
				if (children_idx[i] == key) {
					children[i].decRefc();
					children[i] = value;
					nodes.add(this, true, false);
					return;
				}
			}
			children_idx = Arrays.copyOf(children_idx, children.length + 1);
			children = Arrays.copyOf(children, children.length + 1);
			children_idx[children.length - 1] = key;
			children[children.length - 1] = value;
			nodes.add(this, true, false);
		}

		public void setIsEnd(boolean v) {
			nodes.remove(this, true, false);
			isEnd = v;
			nodes.add(this, true, false);
		}

		public Node getChild(int key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; ++i)
				if (children_idx[i] == key)
					return children[i];
			return null;
		}

		public Node copy() {
			Node ret = new Node();
			ret.children = Arrays.copyOf(children, children.length);
			ret.children_idx = Arrays.copyOf(children_idx, children.length);
			ret.isEnd = isEnd;
			for (Node c : children)
				c.incRefc();
			return ret;
		}

		private void _getPfx(LinkedList<Result> ret, String r, int dist) {
			if (isEnd) {
				Result res = getResult(r, dist, true);
				if (res != null)
					ret.add(res);
			}
			getPfx(ret, r, dist);
		}

		public void getPfx(LinkedList<Result> ret, String r, int dist) { // Warning, potential stack overflow.
			for (int i = 0; i < children.length; ++i)
				children[i]._getPfx(ret, r + new String(children_idx, i, 1), dist); // TODO Should we use codepoint array instead?
		}

		@Override
		public int hashCode() {
			int ret = 0;
			for (int i = 0; i < children.length; ++i) // Must be order-independent!
				ret ^= System.identityHashCode(children[i]) + children_idx[i];
			return ret ^ (isEnd ? 1234 : 4321);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			Node n = (Node) o;
			if (isEnd != n.isEnd)
				return false;
			if (children.length != n.children.length)
				return false;
			for (int i = 0; i < children.length; ++i) { // Compares array regardless of order.
				boolean found = false;
				for (int j = 0; j < n.children.length; ++j)
					if (children[i] == n.children[j] && children_idx[i] == n.children_idx[j])
						found = true;
				if (!found)
					return false;
			}
			return true;
		}
	}

	public Result getResult(String word, int dist, boolean isPrefix) {
		Integer score = scores.get(word);
		if (score == null)
			return null;
		Result res = new Result();
		res.word = word;
		res.score = score;
		res.dist = dist;
		res.isPrefix = isPrefix;
		return res;
	}

	/* Insert similarly to https://doi.org/10.1016/S0304-3975(02)00571-6 */
	/* But with DedupMap to speed up finding similar nodes. */
	public void add(String str, int score) {
		ArrayList<Node> st = new ArrayList<Node>();
		int[] s = Util.stringToCodePoints(str);
		Node n = root;
		/* Split off existing nodes and create new ones. */
		st.add(n);
		for (int i = 0; i < s.length; ++i) {
			Node nn = n.getChild(s[i]);
			if (nn == null) {
				nn = new Node();
				n.setChild(s[i], nn);
				n = nn;
			} else {
				nn = nn.copy(); // We must copy it, otherwise we will mess up other words.
				n.setChild(s[i], nn);
				n = nn;
			}
			st.add(n);
		}
		n.setIsEnd(true);
		/* In reverse order, replace them with equivalent nodes. */
		for (int i = st.size() - 1; i > 0; --i) {
			Node on = st.get(i);
			if ((n = nodes.get(on, false, true)) != null) {
				Node p = st.get(i - 1);
				p.setChild(s[i - 1], n);
			}
		}
		Integer osc = scores.get(str);
		if (osc == null || osc < score) // Only add if score is higher than existing.
			scores.put(str, score);
	}

	public void finish() { // Stop ability to add but reduce memory use.
		nodes = null;
	}

	/* Used only by find function. */
	private void _find(LinkedList<Result> ret, Node n, int[][] d, int[] a, int[] b, int la, int lb, int dist, boolean pfx) {
		for (int j = 1; j <= lb; ++j) {
			d[la][j] = d[la - 1][j] + 1;
			d[la][j] = Math.min(d[la][j], d[la][j - 1] + 1);
			d[la][j] = Math.min(d[la][j], d[la - 1][j - 1] + ((a[la - 1] != b[j - 1]) ? 1 : 0));
			if (la > 1 && j > 1 && a[la - 1] == b[j - 2] && a[la - 2] == b[j - 1])
				d[la][j] = Math.min(d[la][j], d[la - 2][j - 2] + 1);
		}
		int mindist = dist + 1;
		for (int i = Math.max(0, la - dist), e = Math.min(lb, la + dist); i <= e; ++i)
			mindist = Math.min(mindist, d[la][i]);
		if (mindist <= dist) {
			for (int i = 0; i < n.children.length; ++i) {
				a[la] = n.children_idx[i];
				_find(ret, n.children[i], d, a, b, la + 1, lb, dist, pfx);
			}
		}
		if (d[la][lb] == dist) { // Use <= to get results up to a particular distance.
			if (n.isEnd) {
				Result res = getResult(new String(a, 0, la), d[la][lb], false);
				if (res != null)
					ret.add(res);
			}
			if (pfx)
				n.getPfx(ret, new String(a, 0, la), d[la][lb]);
		}
	}

	/* Search based on modified Wagner-Fischer algorithm. */
	/* Distance used is restricted (optimal string alignment) Damerau-Levenshtein distance. */
	public LinkedList<Result> find(String s, int dist, boolean pfx) { // Warning, recursive, catch stackoverflow.
		LinkedList<Result> ret = new LinkedList<Result>(); // TODO use arraylist if no sorting done here yet
		int[] b = Util.stringToCodePoints(s); // TODO How to deal with capitalization? Maybe store map with only words that have alternate capitalization / chars
		int[] a = new int[b.length + dist + 1]; // TODO The problem with capitals is have to fetch the capitalized word from dict. i guess. Or maybe just consider incorrect capitals as any other mistake?
		int[][] d = new int[b.length + dist + 2][b.length + 1]; // The max for la is b.length() + di.
		for (int i = 0; i <= b.length + dist + 1; ++i) // If j == 0 the distance is i.
			d[i][0] = i;
		for (int j = 1; j <= b.length; ++j) // If i == 0 the distance is j, and (0, 0) is already set.
			d[0][j] = j;
		for (int i = 0; i < root.children.length; ++i) {
			a[0] = root.children_idx[i];
			_find(ret, root.children[i], d, a, b, 1, b.length, dist, pfx);
		}
		return ret;
	}

	/* Find but stop searching when enough items are found. */
	public LinkedList<Result> find(String s, int maxd, boolean pfx, int limit) {
		LinkedList<Result> ret = new LinkedList<Result>();
		for (int i = 0; i <= maxd && ret.size() < limit; ++i)
			ret.addAll(find(s, i, pfx)); // TODO What to do with duplicates due to prefix? Perhaps just document it if sort and filter is to be external to this class.
		Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) { // TODO Do this better.
				return b.score - a.score;
			}
		});
		Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) {
				return a.dist - b.dist;
			}
		});
		/*Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) {
				return (a.isPrefix ? 1 : 0) + (b.isPrefix ? -1 : 0);
			}
		});*/
		return ret;
	}
}
