package com.example.netman.mboard;

@SuppressWarnings("unchecked")
public class DedupMap<T> {
	private Object[] data = new Object[256];
	private int size = 0;

	private int gethc(T obj, boolean exact, boolean equiv) {
		int h = Math.abs(Util.fnv32(obj.hashCode())) % data.length; // The fnv part compensates for bad hashCode implementations.
		while (data[h] != null && !((exact && data[h] == obj) || (equiv && data[h].equals(obj) && data[h] != obj)))
			h = (h + 1) % data.length;
		return h;
	}

	public T add(T obj, boolean exact, boolean equiv) { // The exact and equiv determine when to get instead of add.
		int h = gethc(obj, exact, equiv);
		if (data[h] != null)
			return (T) data[h];
		if (data.length * 8 / 10 < size + 1) { // Double the array if load factor is over 0.8.
			Object[] odata = data;
			data = new Object[data.length * 2];
			size = 0;
			for (Object o : odata)
				if (o != null)
					add((T) o, false, false);
			h = gethc(obj, exact, equiv);
		}
		data[h] = obj;
		++size;
		return obj;
	}

	public T get(T obj, boolean exact, boolean equiv) {
		return (T) data[gethc(obj, exact, equiv)];
	}

	public void remove(T obj, boolean exact, boolean equiv) {
		int hs = gethc(obj, exact, equiv);
		if (data[hs] != null) {
			data[hs] = null;
			// Make sure all chained items are still reachable.
			for (int h = (hs + 1) % data.length; data[h] != null; h = (h + 1) % data.length) {
				if (gethc((T) data[h], true, false) == hs) {
					data[hs] = data[h];
					data[h] = null;
					hs = h;
				}
			}
			--size;
		}
	}
}
