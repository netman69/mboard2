package com.example.netman.mboard;

import android.content.Context;
import android.os.AsyncTask;
import java.util.LinkedList;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Dictionary extends AsyncTask<InputStreamReader, Void, Void> {
	private Dawg dawg = new Dawg();

	public Dictionary(Context ctx) {
		InputStream is = ctx.getResources().openRawResource(R.raw.en_part); // TODO user dictionaries
		execute(new InputStreamReader(is));
	}

	public LinkedList<Dawg.Result> find(String s, int maxd, boolean pfx, int limit) {
		LinkedList<Dawg.Result> ret;
		synchronized (dawg) {
			ret = dawg.find(s, maxd, pfx, limit); // TODO try/catch for stack overflow
		}
		return ret;
	}

	@Override
	protected Void doInBackground(InputStreamReader... params) {
		try (BufferedReader br = new BufferedReader(params[0])) {
			String line;
			int s = 99999999;
			while ((line = br.readLine()) != null) {
				synchronized (dawg) {
					dawg.add(line, s--); // TODO try/catch for stack overflow
				}
			}
			dawg.finish();
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Stop loading indication.
	}
}
