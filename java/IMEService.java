package com.example.netman.mboard;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

/* TODO
 * - profiles that encompass all or nearly all settings so hw key remaps etc can be switched easily
 * - if enter delete key by slide, delete only one char when pointer up
 * - if press down in delete key and move off it, select text to be deleted by how far moved
 * - space bar to move cursor when go left/right (also only when initiated by press down on it?)
 * - switch language button
 * - popup things with multiple long-press options
 * - symbol overlay popping up on physical keycombo, like bb classic (separate key layout, perhaps multi page)
 *     perhaps show overlay if press and let go of sym but not if hold and then press other key
 * - ability to have split layout mode in landscape so thumbs don't need to go to center
 *     configurable layouts should exist, but this means some may want separate layout for landscape
 * - ctrl-arrow to jump words
 * - alt-codes for ascii
 */

public class IMEService extends InputMethodService {
	private KeyboardView kv;
	private SuggestView sv;
	private InputConnection ic;
	private static int maxWordLen = 1024;

	@Override
	public View onCreateInputView() {
		View v = getLayoutInflater().inflate(R.layout.keyboard, null);
		kv = (KeyboardView) v.findViewById(R.id.kbdview);
		sv = (SuggestView) v.findViewById(R.id.suggestview);
		kv.setIMEService(this);
		sv.setIMEService(this);
		kv.setLayout();
		return v;
	}

	@Override
	public void onStartInputView(EditorInfo info, boolean restarting) {
		// TODO deal with input type, in case it is number or so
		ic = getCurrentInputConnection();
	}

	@Override
	public void onFinishInputView(boolean finishingInput) {
		ic = null;
	}

	public void switchIme() {
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		imm.showInputMethodPicker();
	}

	public void insert(String s) {
		ic.commitText(s, 1);
		update();
	}

	public void backspace() {
		ExtractedText et = ic.getExtractedText(new ExtractedTextRequest(), 0); // TODO maybe this should be abstracted
		if (et.selectionStart != et.selectionEnd)
			ic.commitText("", 1);
		else ic.deleteSurroundingText(1, 0); // TODO deal with surrogate pairs
		update();
	}

	public void enter() {
		ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
		update();
	}

	private void update() {
		String str = ic.getTextBeforeCursor(maxWordLen, 0).toString(); // TODO deal with edge case when maxWordLen too short.
		int start = str.length(); // TODO what if text was selected?
		int end = start;
		str += ic.getTextAfterCursor(maxWordLen, 0).toString();
		for (; start > 0; --start) {
			char c = str.charAt(start - 1);
			if (c == ' ' || c == '\t') // TODO is this problem with unicode?
				break;
		}
		for (int s = str.length(); end < s - 1; ++end) {
			char c = str.charAt(end + 1);
			if (c == ' ' || c == '\t')
				break;
		}
		String word = str.substring(start, end);
		sv.setWord(word);
	}

	public void setCorrection(String w) {
		// TODO Deal with selection and stuff.
		String str = ic.getTextBeforeCursor(maxWordLen, 0).toString(); // TODO deal with edge case when maxWordLen too short.
		int start = str.length(), end = 0;
		for (; start > 0; --start) {
			char c = str.charAt(start - 1);
			if (c == ' ' || c == '\t')
				break;
		}
		start = str.length() - start;
		str = ic.getTextAfterCursor(maxWordLen, 0).toString();
		for (int s = str.length(); end < s - 1; ++end) {
			char c = str.charAt(end + 1);
			if (c == ' ' || c == '\t')
				break;
		}
		ic.deleteSurroundingText(start, end);
		insert(w);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (super.onKeyDown(keyCode, event))
			return true;
		Log.w("KEYKEY", Integer.toString(keyCode));
		/*ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DPAD_DOWN));
		return true;*/
		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (super.onKeyUp(keyCode, event))
			return true;
		/*ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DPAD_DOWN));
		return true;*/
		return false;
	}

	// TODO i don't think this is used
	/*private boolean caps = false;

	public void onKey(int primaryCode, int[] keyCodes) {
		InputConnection ic = getCurrentInputConnection();
		switch (primaryCode){
			case Keyboard.KEYCODE_DELETE :
				ic.deleteSurroundingText(1, 0);
				break;
			case Keyboard.KEYCODE_SHIFT:
				caps = !caps;
				// TODO
				break;
			case Keyboard.KEYCODE_DONE:
				ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
				break;
			default:
				char code = (char) primaryCode;
				if(Character.isLetter(code) && caps)
					code = Character.toUpperCase(code);
				ic.commitText(String.valueOf(code), 1);
		}
	}*/
}
