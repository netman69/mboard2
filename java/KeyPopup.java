package com.example.netman.mboard;

import android.widget.PopupWindow;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class KeyPopup extends PopupWindow {
	private Context ctx;
	TextView tv;

	public KeyPopup(Context context) {
		super(context);
		ctx = context;
		init();
	}

	private void init() {
		tv = new TextView(ctx);
		tv.setText("");
		tv.setTextSize(20f / tv.getResources().getDisplayMetrics().density);
		tv.setPadding(5, 5, 5, 30); // TODO make the layout/style proper.
		setContentView(tv);
		setClippingEnabled(false);
		setAnimationStyle(0);
	}

	public void setText(CharSequence text) {
		tv.setText(text);
	}
}
