package com.example.netman.mboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Handler;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import java.util.ArrayList;
import java.util.LinkedList;
import java.lang.Math;

import android.util.Log;

public class SuggestView extends LinearLayout {
	private Context ctx;
	private Dictionary dict;
	private IMEService svc;

	public SuggestView(Context context) {
		super(context);
		ctx = context;
		init();
	}

	public SuggestView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ctx = context;
		init();
	}

	public SuggestView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		ctx = context;
		init();
	}

	private void init() { // TODO
		dict = new Dictionary(ctx);
		addView(new Suggestion("-")); // To prevent it collapsing. TODO
	}

	public void setIMEService(IMEService svc) {
		this.svc = svc;
	}

	public void setWord(String word) {
		removeAllViews();
		if (word.length() >= 1) {
			LinkedList<Dawg.Result> res = dict.find(word, 3, false, 4); // TODO
			for (int i = 0; i < res.size() && i < 4; ++i)
				addView(new Suggestion(res.get(i).word));
		}
		if (getChildCount() == 0)
			addView(new Suggestion("-")); // To prevent it collapsing. TODO
	}

	private class Suggestion extends TextView {
		public Suggestion(String text) {
			super(ctx);
			init(text, 1.0f);
		}

		private void init(String text, float weight) {
			setText(text);
			setTextSize(15f / getResources().getDisplayMetrics().density);
			setFocusable(false);
			setFocusableInTouchMode(false);
			setTransformationMethod(null); // To stop letters being all-caps.
			setAllCaps(false);
			setTextAlignment(TEXT_ALIGNMENT_CENTER);
			setPadding(5, 5, 5, 5); /* Left, Top, Right, Bottom */
			setBackgroundResource(R.drawable.key);
			setTextColor(getResources().getColor(R.color.colorPrimary));
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, weight);
			params.setMargins(0, 0, 0, 0);
			setLayoutParams(params);
			setSingleLine();
			setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					svc.setCorrection(getText().toString() + " ");
				}
			});
		}
	}
}
