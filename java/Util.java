package com.example.netman.mboard;

public class Util {
	public static int fnv32(int d) { // Calculate 32bit FNV-1a hash of 32bit number.
		int ret = 0x811C9DC5; // TODO use crc64 instead
		ret = (ret ^ ((d >>>  0) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>>  8) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>> 16) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>> 24) & 0xFF)) * 0x1000193;
		return ret;
	}

	public static int[] stringToCodePoints(String s) {
		int[] ret = new int[s.codePointCount(0, s.length())];
		for (int i = 0; i < ret.length; ++i)
			ret[i] = s.codePointAt(s.offsetByCodePoints(0, i));
		return ret;
	}
}
