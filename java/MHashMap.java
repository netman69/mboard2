package com.example.netman.mboard;

@SuppressWarnings("unchecked")
public class MHashMap<K, V> {
	private Object[] keys = new Object[256];
	private Object[] data = new Object[256];
	private int size = 0;

	private int gethc(K key) {
		int h = Math.abs(Util.fnv32(key.hashCode())) % keys.length; // The fnv part compensates for bad hashCode implementations.
		while (keys[h] != null && !keys[h].equals(key))
			h = (h + 1) % keys.length;
		return h;
	}

	public void put(K key, V value) {
		int h = gethc(key);
		if (keys[h] == null)
			++size;
		if (data.length * 8 / 10 < size + 1) { // Double the array if load factor is over 0.8.
			Object[] okeys = keys;
			Object[] odata = data;
			keys = new Object[keys.length * 2];
			data = new Object[data.length * 2];
			size = 0;
			for (int i = 0; i < okeys.length; ++i)
				if (okeys[i] != null)
					put((K) okeys[i], (V) odata[i]);
			h = gethc(key);
		}
		keys[h] = key;
		data[h] = value;
	}

	public V get(K key) {
		return (V) data[gethc(key)];
	}

	public void remove(K key) {
		int hs = gethc(key);
		if (keys[hs] != null) {
			keys[hs] = null;
			data[hs] = null;
			// Make sure all chained items are still reachable.
			for (int h = (hs + 1) % keys.length; keys[h] != null; h = (h + 1) % keys.length) {
				if (gethc((K) keys[h]) == hs) {
					keys[hs] = keys[h];
					data[hs] = data[h];
					keys[h] = null;
					data[h] = null;
					hs = h;
				}
			}
			--size;
		}
	}
}
