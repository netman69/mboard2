#!/bin/sh
# you need wget, unzip and openjdk8 to get started
# on freebsd some of the tools need linux_base-c7
# for different versions of stuff look at https://androidsdkmanager.azurewebsites.net/
# if there's complaints of libc++.so not existing, it is in the build-tools package and you simply have to have it in LD_LIBRARY_PATH
# additionally on freebsd you can't use adb from the platform-tools, take the one from ports instead
#   use the android-tools-adb-9.0.0.r3_4 with portdowngrade if adb install always fails

try() {
	if ! ( $* ); then
		echo # so stderr doesn't overwrite stdout
		>&2 echo "* Script stopped due to error."
		exit 1
	fi
}

fetch() {
	try rm -f "$1" # in case an incomplete download exists
	try wget "https://dl.google.com/android/repository/$1"
	try unzip "$1"
	try rm -f "$1"
}

newdir() {
	if [ ! -d "$1" ]; then try mkdir "$1"; fi
	if ! cd "$1"; then try false; fi
}


# create a directory for our downloads
newdir tools

# get the platform tools
#if [ ! -d "platform-tools" ]; then
#	>&2 echo "* Fetching platform-tools."
#	fetch "platform-tools_r29.0.4-linux.zip"
#else
#	>&2 echo "* Platform-tools already present."
#fi

# get the build tools
if [ ! -d "build-tools" ]; then
	>&2 echo "* Fetching build-tools."
	newdir build-tools
	fetch "build-tools_r29.0.2-linux.zip"
	cd ..
else
	>&2 echo "* Build-tools already present."
fi

# get the SDK
if [ ! -d "sdk" ]; then
	>&2 echo "* Fetching SDK."
	newdir sdk
	fetch "platform-28_r06.zip"
	cd ..
else
	>&2 echo "* SDK already present."
fi

>&2 echo "* Successfully fetched build requirements."

