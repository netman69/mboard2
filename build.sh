pkgn="com/example/netman/mboard"

try() {
	if ! ( $* ); then
		echo # so stderr doesn't overwrite stdout
		>&2 echo "* Script stopped due to error."
		exit 1
	fi
}

newdir() {
	if [ ! -d "$1" ]; then try mkdir "$1"; fi
}

buildtools="./tools/build-tools/android-10"
sdk="./tools/sdk/android-9/"

# Set LD_LIBRARY_PATH so libc++.so is found.
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$buildtools/lib64/"

# Compile resources.
>&2 echo "* Compiling resources..."
newdir build
try "$buildtools/aapt2" compile --dir res/ -o build/res.zip
try "$buildtools/aapt2" link -o build/res.apk --java build/ --manifest AndroidManifest.xml -I "$sdk/android.jar" build/res.zip

# Compile classes and dex.
>&2 echo "* Compiling java..."
newdir build/classes
try "javac" -g:none -cp "$sdk/android.jar" -d build/classes/ java/*.java build/$pkgn/R.java
try "java" -jar "$buildtools/lib/dx.jar" --dex --output=build/classes.dex build/classes/

# Package platform base and dex.
>&2 echo "* Creating apk..."
try "$buildtools/aapt2" link -o build/unaligned.apk --manifest AndroidManifest.xml -I "$sdk/android.jar" build/res.zip
try zip -uj build/unaligned.apk build/classes.dex

# Align apk.
>&2 echo "* Creating aligned apk..."
try "$buildtools/zipalign" -f -v -p 4 build/unaligned.apk build/output.apk
# TODO

# Sign apk.
>&2 echo "* Signing aligned apk..."
try java -jar "$buildtools/lib/apksigner.jar" sign --verbose --ks release.jks --ks-pass file:release.jks.pass build/output.apk
# to generate a keystore use the following command:
#   keytool -genkey -v -keystore release.jks -keyalg RSA -keysize 2048 -validity 10000

